package tanks2019

class CommandException : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
    constructor(message: String, exception: Exception) : super(message, exception)
    constructor(exception: Exception) : super(exception)
}
