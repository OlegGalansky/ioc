package tanks2019.ioc

import tanks2019.Command
import tanks2019.IoCStrategy

internal class RegisterCommand(private val key: String, private val command: IoCStrategy) : Command {
    override fun execute() {
        IoCHash.resolveOrExecute(key, command)
    }
}
