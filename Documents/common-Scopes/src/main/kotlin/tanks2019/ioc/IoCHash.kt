package tanks2019.ioc

import tanks2019.IoCException
import tanks2019.IoCStrategy

class IoCHash {
    companion object {
        private val hash = HashMap<String, IoCStrategy>()

        fun resolveOrExecute(key: String, strategy: IoCStrategy): IoCStrategy {
            return hash.getOrPut(key, { strategy })
        }

        init {
            hash.put("IoC.Register", { k: Array<out Any> ->
                try {
                    @Suppress("UNCHECKED_CAST")
                    return@put RegisterCommand(k[0] as String, k[1] as IoCStrategy)
                } catch (e: ClassCastException) {
                    throw IoCException(
                        "Wrong type of argument(s) for IoC.Register dependency. Argument 2 should be String. Argument 3 should be IoCStrategy.",
                        e
                    )
                } catch (e: IndexOutOfBoundsException) {
                    throw IoCException(
                        String.format(
                            "Wrong number of arguments for IoC.Register dependency. It should be three, instead of {0}",
                            k.size
                        ), e
                    )
                }
            })
        }
    }
}
