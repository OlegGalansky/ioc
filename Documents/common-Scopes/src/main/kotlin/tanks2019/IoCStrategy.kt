package tanks2019

typealias IoCStrategy = (args: Array<out Any>) -> Any
