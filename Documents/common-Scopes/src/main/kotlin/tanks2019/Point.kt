package tanks2019

/**
 * Перегружает оператор += для Векторов.
 * В цикле для каждого элемента двух массивов мы применяем оператор +=, позволяя при перегрузке складывать каждый элемент массива друг с другом.
 * @param x - Массив типа IntArray(Подается на вход в качестве параметра конструктора).
 * @param other - Массив типа IntArray(Подается на вход в качестве параметра функции plusAssign).
 */
class Point(var x : IntArray) {
    operator fun plusAssign(other: Point) : Unit  {
        for(i in 0 until x.size) {
            this.x[i] += other.x[i]
        }
    }
}