package tanks2019

/**
 * Base interface for all operations.
 */
interface Command {
    /**
     * Execute command.
     * @throws CommandException if command could not be executed.
     */
    fun execute()
}
