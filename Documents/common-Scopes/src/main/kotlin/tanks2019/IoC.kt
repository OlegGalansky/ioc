package tanks2019

import tanks2019.ioc.IoCHash

inline fun <reified T> resolve(key: String, vararg args: Any): T {
    return IoCHash.resolveOrExecute(key, { throw IoCException(String.format("IoC Dependency {0} is not found.", key)) }) (args) as T
}
