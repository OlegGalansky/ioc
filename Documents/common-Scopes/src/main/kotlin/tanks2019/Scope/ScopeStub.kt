package tanks2019.Scope

/**
 * Заглушка,котороая вбырасывает исклучения,если в дереве скоупов,метода регистрации и разрешения не существует.
 */

class ScopeStub<V>: Scope<V> {
    override fun resolve(key: String): V {
        throw NoSuchMethodException("\n" +
                "There is no 'resolve' method for this branch of scopes")
    }

    override fun register(key: String, strategy: V) {
        throw  NoSuchMethodException("\n" +
                "There is no 'register' method for this scopes branch")
    }
}