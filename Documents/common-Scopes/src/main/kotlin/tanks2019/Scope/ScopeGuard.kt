package tanks2019.Scope

import tanks2019.Command
import tanks2019.IoCStrategy

/**
 *Класс реализующий команду,которую необходимо использовать при создании новой ветки Scope'a(области видимости игры).
 *Ожидаемое поведение,заключается в том,чтобы передавать в конструктор класса старый Scope,чтобы сохранить его и при необходимости вернутся к нему.
 * @param scope - Скоуп который нужно передать для сохранения.
 * @author Galansky Oleg
 */

class ScopeGuard(private val scope: Scope<IoCStrategy>): Command {
    override fun execute() {
        ScopesManager.setCurrent(scope)
    }
}