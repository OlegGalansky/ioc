package tanks2019.Scope

class ScopeDefault<V>(private val parent: Scope<V>) : Scope<V>{

    private val strategyMap = HashMap<String,V>()

    override fun resolve(key: String): V {
        return strategyMap.getOrElse(key, {parent.resolve(key)})
    }

    override fun register(key: String, strategy: V) {
        strategyMap[key] = strategy
    }
}