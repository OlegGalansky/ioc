package tanks2019.Scope

import tanks2019.IoCException
import tanks2019.IoCStrategy
import tanks2019.ioc.RegisterCommand
import kotlin.concurrent.getOrSet

internal class ScopesManager {
    companion object {
        private val threadStore = ThreadLocal<Scope<IoCStrategy>>()
        fun current(): Scope<IoCStrategy> {
            return threadStore.getOrSet { default() }
        }

        internal fun setCurrent(scope: Scope<IoCStrategy>) {
            threadStore.set(scope)
        }

        private val root: Scope<IoCStrategy> = ScopeDefault(ScopeStub())
        private var default: () -> Scope<IoCStrategy> = { root }

        init {
            setCurrent(root)

            root.register("IoC.Register", { k: Array<out Any> ->
                try {
                    @Suppress("UNCHECKED_CAST")
                    return@register RegisterCommand(
                        k[0] as String, k[1] as IoCStrategy
                    )
                } catch (e: ClassCastException) {
                    throw IoCException(
                        "Wrong type of argument(s) for IoC.Register " +
                                "dependency . Argument 2 should be String . Argument 3 should be IoCStrategy .", e
                    )
                } catch (e: IndexOutOfBoundsException) {
                    throw IoCException(
                        String.format(
                            "Wrong number of arguments for IoC." +
                                    "Register dependency . It should be three, instead of { 0 }", k.size
                        ), e
                    )
                }
            })



            root.register("setupScope", { k: Array<out Any> ->

                try {
                    val oldScope = k[0] as Scope<IoCStrategy>
                    try {
                        setCurrent(k[1] as Scope<IoCStrategy>)
                        return@register ScopeGuard(oldScope)
                    } catch (e: ClassCastException) {
                        throw IoCException(
                            "Wrong type of argument for new scope in setupScope." +
                                    "Argument should be Scope<IoCStrategy>", e
                        )
                    }
                } catch (e: ClassCastException) {
                    throw IoCException(
                        "Wrong type of argument for old scope in setupScope." +
                                "Argument should be Scope<IoCStrategy>", e
                    )
                }
            })

            root.register("currentScope", { _: Array<out Any> ->
                return@register current()
            })

            root.register("newScope", { k: Array<out Any> ->
                try {
                    return@register ScopeDefault(k[0] as Scope<IoCStrategy>)
                } catch (e: ClassCastException) {
                    throw IoCException(
                        "Wrong type of argument for new scope in newScope." +
                                "Argument should be Scope<IoCStrategy>", e
                    )
                }
            })

        }
    }
}
