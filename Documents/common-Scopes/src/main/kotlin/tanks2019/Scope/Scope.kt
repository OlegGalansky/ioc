package tanks2019.Scope

interface Scope<V> {
    fun resolve(key: String): V
    fun register(key: String, strategy: V)
}