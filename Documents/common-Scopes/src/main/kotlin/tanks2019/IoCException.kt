package tanks2019

class IoCException : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
    constructor(message: String, exception: Exception) : super(message, exception)
    constructor(exception: Exception) : super(exception)
}
