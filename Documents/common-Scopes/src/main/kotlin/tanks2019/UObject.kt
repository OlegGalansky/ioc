package tanks2019

/**
 * Interface of universal object.
 */
interface UObject {
    /**
     * Set value of the property.
     * @param key Name of the property.
     * @param value New value of the property.
     * @throws UObjectException if the property could not write.
     */
    fun set(key: String, value: Any)

    /**
     * Get value of the property.
     * @param key Name of the property.
     * @return Current value of the property.
     * @throws UObjectException if the property could not read.
     */
    fun get(key: String): Any
}
