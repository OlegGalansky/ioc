package tanks2019

import kotlin.test.assertEquals
import org.junit.Test

class IoCTest {
    @Test
    fun IoCShouldResolveRegisteredDependency() {
        resolve<Command>("IoC.Register", "test", { _: Array<out Any > -> 1 }).execute()

        assertEquals(1, resolve<Int>("test"))
    }

    @Test(expected = IoCException::class)
    fun IoCShouldThrowExceptionOnResolvingUnregisteredDependency() {
        resolve<Int>("UnregisteredKey")
    }

    @Test(expected = IoCException::class)
    fun SecondArgumentForIocRegisterDependencyShouldBeString() {
        resolve<Int>("IoC.Register", 1, {})
    }

    @Test(expected = IoCException::class)
    fun ThirdArgumentForIocRegisterDependencyShouldBeString() {
        resolve<Int>("IoC.Register", "test", {})
    }

    @Test(expected = IoCException::class)
    fun resolveShouldHaveNotOneArgument() {
        resolve<Int>("IoC.Register")
    }

    @Test(expected = IoCException::class)
    fun resolveShouldHaveNotTwoArguments() {
        resolve<Int>("IoC.Register", "test")
    }
}
