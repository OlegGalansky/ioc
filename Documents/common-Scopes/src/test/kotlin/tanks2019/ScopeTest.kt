package tanks2019

import kotlin.test.assertEquals
import org.junit.Test
import tanks2019.Scope.*

class InSeparateScope {
    companion object {
        fun execute(arg: () -> Unit) {
            val parent = resolve<Scope<IoCStrategy>>("ScopesManager.current")
            val scope = resolve<Scope<IoCStrategy>>("ScopesManager.setupScope", parent)
            val guarder = resolve<Command>("ScopesManager.currentScope", scope)
            try {
                arg()
            } finally {
                guarder.execute()
            }
        }
    }
}

class ScopeTest {
    @Test
    fun a(){
        ScopesManager.current()
        val scope = InSeparateScope.execute {

        }

    }
}

